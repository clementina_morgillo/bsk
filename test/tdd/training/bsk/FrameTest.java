package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	//Test user story 1
	public void testFrame() throws BowlingException{
		
		int firstThrow = 4;
		int secondThrow = 5;
		
		try {
			
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, f.getFirstThrow());
			assertEquals(secondThrow, f.getSecondThrow());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//Test user story 2
	public void testFrameScore() throws BowlingException{
		
		int firstThrow = 4;
		int secondThrow = 5;
		
		try {
			
			Frame f = new Frame(firstThrow, secondThrow);
			System.out.println("User story 2 - Score: " + f.getScore());
			assertEquals(firstThrow+secondThrow, f.getScore());
		
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//Test user story 3
	public void testGame() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			game.addFrame(new Frame(1, 5));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
		
			assertEquals(new Frame(3,6).getFirstThrow(), game.getFrameAt(3).getFirstThrow());
			assertEquals(new Frame(3,6).getSecondThrow(), game.getFrameAt(3).getSecondThrow());
			assertEquals(new Frame(2,6).getFirstThrow(), game.getFrameAt(9).getFirstThrow());
			assertEquals(new Frame(2,6).getSecondThrow(), game.getFrameAt(9).getSecondThrow());
			
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}		
					
	}
	
	@Test
	//Test user story 4
	public void testGameScore() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(1,5);
			game.addFrame(f1);
			Frame f2 = new Frame(3,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 4 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}		
					
	}
	
	@Test
	//Test user story 5
	public void testSpare() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(1,9);
			game.addFrame(f1);
			Frame f2 = new Frame(3,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 5 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}					
	}
	
	@Test
	//Test user story 6
	public void testStrike() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(10,0);
			game.addFrame(f1);
			Frame f2 = new Frame(3,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 6 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}		
					
	}
	
	@Test
	//Test user story 7
	public void testStrikeAndSpare() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(10,0);
			game.addFrame(f1);
			Frame f2 = new Frame(4,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 7 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}		
					
	}
	
	@Test
	//Test user story 8
	public void testMultipleStrikes() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(10,0);
			game.addFrame(f1);
			Frame f2 = new Frame(10,0);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 8 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}		
					
	}
	
	@Test
	//Test user story 9
	public void testMultipleSpares() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(8,2);
			game.addFrame(f1);
			Frame f2 = new Frame(5,5);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,6);
			game.addFrame(f10);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
			
			System.out.println("User story 9 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}					
	}

	
	@Test
	//Test user story 10
	public void testSpareAsTheLastFrame() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(1,5);
			game.addFrame(f1);
			Frame f2 = new Frame(3,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(2,8);
			game.addFrame(f10);
			
			//bonus throw
			game.setFirstBonusThrow(7);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow()+7;
			
			System.out.println("User story 10 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}					
	}
	
	@Test
	//Test user story 11
	public void testStrikeAsTheLastFrame() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(1,5);
			game.addFrame(f1);
			Frame f2 = new Frame(3,6);
			game.addFrame(f2);
			Frame f3 = new Frame(7,2);
			game.addFrame(f3);
			Frame f4 = new Frame(3,6);
			game.addFrame(f4);
			Frame f5 = new Frame(4,4);
			game.addFrame(f5);
			Frame f6 = new Frame(5,3);
			game.addFrame(f6);
			Frame f7 = new Frame(3,3);
			game.addFrame(f7);
			Frame f8 = new Frame(4,5);
			game.addFrame(f8);
			Frame f9 = new Frame(8,1);
			game.addFrame(f9);
			Frame f10 = new Frame(10,0);
			game.addFrame(f10);
			
			//bonus throws
			game.setFirstBonusThrow(7);
			game.setSecondBonusThrow(2);
			
			int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow()+7+2;
			
			System.out.println("User story 11 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}					
	}
	
	@Test
	//Test user story 12
	public void testBestScore() throws BowlingException{
			
		// It initializes an empty bowling game.
		Game game = new Game();
	
		try {
			// It adds 10 frames to this bowling game
			Frame f1 = new Frame(10,0);
			game.addFrame(f1);
			Frame f2 = new Frame(10,0);
			game.addFrame(f2);
			Frame f3 = new Frame(10,0);
			game.addFrame(f3);
			Frame f4 = new Frame(10,0);
			game.addFrame(f4);
			Frame f5 = new Frame(10,0);
			game.addFrame(f5);
			Frame f6 = new Frame(10,0);
			game.addFrame(f6);
			Frame f7 = new Frame(10,0);
			game.addFrame(f7);
			Frame f8 = new Frame(10,0);
			game.addFrame(f8);
			Frame f9 = new Frame(10,0);
			game.addFrame(f9);
			Frame f10 = new Frame(10,0);
			game.addFrame(f10);
			
			//bonus throws
			game.setFirstBonusThrow(10);
			game.setSecondBonusThrow(10);
			
			int score = 300; 
			
			System.out.println("User story 12 - Bowling Score: " + game.calculateScore());
			
			assertEquals(game.calculateScore(),score);
			
		} catch (BowlingException b) {
			System.err.println(b.getMessage());
		}					
	}
}
