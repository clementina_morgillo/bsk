package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	protected ArrayList<Frame> frames;
	protected int firstBonusThrow;
	protected int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<>(10);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() >= 10) {
			throw new BowlingException("You can't add frame! The limit is 10!");
		} else {
			this.frames.add(frame);
		}
		
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index >= 10 || index < 0) {
			throw new BowlingException("Index out of range!");
		}
		Frame  f = frames.get(index);
		return f;	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}
	
	/**
	 * It sets bonus for a spare frame.
	 * 
	 * @param f Spare frame
	 * @throws BowlingException
	 */
	public void calculateBonusSpare(Frame f) throws BowlingException {
		int index = frames.indexOf(f);
		
		//normal spare
		if(frames.indexOf(f)<9) f.setBonus(this.getFrameAt(index+1).getFirstThrow());
		//spare at the last frame: bonus throw
		else f.setBonus(this.getFirstBonusThrow());
	}
	
	/**
	 * It sets bonus for a strike frame.
	 * 
	 * @param f Strike frame
	 * @throws BowlingException
	 */
	public void calculateBonusStrike(Frame f) throws BowlingException {
		int index = frames.indexOf(f);
			
		if(index<8) {
			Frame nextFrame = this.getFrameAt(index+1);
			
			//double strike
			if(nextFrame.isStrike()) f.setBonus(nextFrame.getFirstThrow() + this.getFrameAt(index+2).getFirstThrow());
			//normal strike
			else f.setBonus(nextFrame.getFirstThrow() + nextFrame.getSecondThrow());	
		}
		
		//double strike at the last frame
		else if(index==8 && this.getFrameAt(index+1).isStrike()) {
			f.setBonus(this.getFrameAt(index+1).getFirstThrow() + this.getSecondBonusThrow());
		}
		//strike at the last frame: two bonus throw
		else f.setBonus(this.getFirstBonusThrow()+this.getSecondBonusThrow());	
	}
	
	
	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		for(Frame f : frames) {
			
			//case spare
			if(f.isSpare()) calculateBonusSpare(f);		
			
			//case strike
			if(f.isStrike()) calculateBonusStrike(f);
			
			score = score + f.getScore();
		}
		
		return score;
	}

}
