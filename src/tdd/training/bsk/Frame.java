package tdd.training.bsk;

public class Frame {

	protected int firstThrow;
	protected int secondThrow;
	protected int bonus;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		
		if(firstThrow + secondThrow > 10 || firstThrow < 0 || secondThrow < 0)
			throw new BowlingException("invalid Throw");
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		// To be implemented
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		// To be implemented
		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return this.bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		int score;
		score = this.firstThrow + this.secondThrow;
		
		if(this.isSpare() || this.isStrike()) {
			score = score + this.getBonus();
		}
		
		return score;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if(this.firstThrow==10) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if(this.firstThrow + this.secondThrow == 10) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Prints details of the frame.
	 *
	 */
	public void print() {
		System.out.println("First Throw: " + this.firstThrow);
		System.out.println("Second Throw: " + this.secondThrow);
		System.out.println("Bonus frame: " + this.bonus);
	}

}
